#!/usr/bin/python

import json
import os
import pandas as pd
import argparse

# Argument handling
parser = argparse.ArgumentParser()
parser.add_argument("--file", help="Canary log file", required=True)
parser.add_argument("--userout", help="File to output usernames", required=False)
parser.add_argument("--passout", help="File to output passwords", required=False)
args = parser.parse_args()

# Initialize blank lists
usernames = []
passwords = [] 

# Read from opencanary.log
if os.path.exists(args.file):
    f = open(args.file, "r")
    data = f.read()
else:
    print("A valid Canary log file is required!")
    exit(1)

dataarray = data.split("\n")

for line in dataarray:
    if "USERNAME" in line:
        uname = json.loads(line)["logdata"]["USERNAME"]
        if uname:
            usernames.append(uname)
    if "PASSWORD" in line:
        passw = json.loads(line)["logdata"]["PASSWORD"]
        if passw:
            passwords.append(passw)

# Remove duplicates
u_series = pd.Series(usernames)
u_series = u_series.drop_duplicates()
usernames = u_series.tolist()

p_series = pd.Series(passwords)
p_series = p_series.drop_duplicates()
passwords = p_series.tolist()

# Output to screen
print("USERNAMES:")
print("\n".join(map(str, usernames)))
print("")
print("PASSWORDS:")
print("\n".join(map(str, passwords)))
print("")

# Write usernames to file if '--userout' is used
if args.userout:
    f = open(args.userout, "w")
    for user in usernames:
        f.write(user + "\n")
    f.close()
    print("Usernames written to: " + args.userout)

# Write passwords to file it '--passout' is used
if args.passout:
    f = open(args.passout, "w")
    for passw in passwords:
        f.write(passw + "\n")
    f.close()
    print("Passwords written to: " + args.passout)
