# Overview
This project extends upon Docker-OpenCanary (https://github.com/trigat/Docker-OpenCanary) and adds a twist... capturing the supplied usernames and passwords attempted against the opencanary Docker container.

This data can be used to create dictionaries based on real-world brute-force attacks. An example use case for these dictionaries is to audit your own users' passwords and, if there's a match, change the password immediately.

# Launch Instructions

Build an image:
```
docker build --rm -t opencanary .
```

Create a container:
```
docker run -dit --network host --name opencanary -v *path-to-opencanary.log*:/var/tmp/opencanary.log opencanary
```

Issue a command to start OpenCanary within the container:
```
docker exec -it opencanary bash -c 'opencanaryd --start'
```

# Pay Dirt

View the usernames and passwords
```
python get-creds.py --file *path-to-opencanary.log*
```

Can also save the usernames and passwords to a text file:
```
python get-creds.py --file *path-to-opencanary.log* --userout *path-to-user-log* --passout *path-to-pass-log*
```
